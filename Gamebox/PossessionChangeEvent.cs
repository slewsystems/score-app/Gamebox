using Gamebox.Competition;

namespace Gamebox
{
    public class PossessionChangeEvent
    {
        public PossessionChangeEvent(IOpponent currentOpponent, IOpponent previousOpponent)
        {
            CurrentOpponent = currentOpponent;
            PreviousOpponent = previousOpponent;
        }
        
        public IOpponent CurrentOpponent { get; protected set; }
        public IOpponent PreviousOpponent { get; protected set; }
    }
}