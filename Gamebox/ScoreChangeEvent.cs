using Gamebox.Competition.Division;

namespace Gamebox
{
    public class ScoreChangeEvent
    {
        public ScoreChangeEvent(IDivision division, int currentScore, int previousScore)
        {
            Division = division;
            CurrentScore = currentScore;
            PreviousScore = previousScore;
        }

        public IDivision Division { get; protected set; }
        public int CurrentScore { get; protected set; }
        public int PreviousScore { get; protected set; }
    }
}