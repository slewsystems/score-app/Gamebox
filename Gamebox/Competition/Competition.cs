using System.Collections.Generic;
using System.Collections.Concurrent;
using Gamebox.Competition.Division;
using Gamebox;
using System;

namespace Gamebox.Competition
{
    public class Competition
    {
        public enum ScoringMode
        {
            /// <summary>
            /// Each division score is a difference so the total score is the sum
            /// <para>Example: Div_A := 1, Div_B := 3 ... Total = 4</para>
            /// </summary>
            SumScore, 
            /// <summary>
            /// Each division score is the total score so the total score is the last division score
            /// <para>Example: Div_A := 3, Div_B := 5 ... Total = 5</para>
            /// </summary>
            LastScore 
        }

        private ISet<IOpponent> _opponents;
        private IOpponent _homeOpponent;
        private IOpponent _visitorOpponent;
        private IOpponent _possessionOpponent;
        public event EventHandler<TimeoutManager.TimeoutChangeEvent> TimeoutChanged;
        public event EventHandler<PossessionChangeEvent> PossessionChanged;
        public event EventHandler<ScoreChangeEvent> ScoreChanged;

        public Competition(
            ISet<IOpponent> opponentSet, 
            IStateMachine divisionStateMachine) 
        {
            Opponents = opponentSet;
            DivisionStateMachine = divisionStateMachine;
            // TODO: listen for division change event
        }
        public ScoringMode ScoreMode { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public DateTime Date { get; set; }
        public ISet<IOpponent> Opponents 
        { 
            get => _opponents;
            private set 
            {
                if (value.Count > 0) { throw new ArgumentException("Opponent set must be empty. To add opponents use AddOpponet()"); }
                _opponents = value;
            }
        }

        public bool AddOpponent(IOpponent opponent)
        {
            if (Opponents.Add(opponent))
            {
                // TODO: test that timeout event gets fired on timeouts
                opponent.TimeoutManager.TimeoutChange += (object sender, TimeoutManager.TimeoutChangeEvent e) => {
                    NotifyTimeoutChange(opponent, e);
                };
                opponent.ScoreChanged += (object sender, ScoreChangeEvent e) => {
                    NotifyScoreChange(opponent, e);
                };
                return true;
            }
            return false;
        }

        public IStateMachine DivisionStateMachine { get; private set; }
        public IOpponent OpponentWithPossession 
        { 
            get => _possessionOpponent;
            set
            {
                if (!Opponents.Contains(value)) { throw new System.ArgumentException("Opponent must exist in set"); }
                var previousOpponent = _possessionOpponent;
                _possessionOpponent = value;

                if (previousOpponent == _possessionOpponent) { return; }
                NotifyPossessionChange(new PossessionChangeEvent(_possessionOpponent, previousOpponent));
            }
        }

        public IOpponent HomeOpponent 
        { 
            // TODO: what should happen if the opponent is removed from the set itself?
            get => _homeOpponent;
            set
            {
                if (!Opponents.Contains(value)) { throw new System.ArgumentException("Opponent must exist in set"); }
                if (AwayOpponent == value) { throw new System.ArgumentException("Opponent cannot be the same as AwayOpponent"); }

                _homeOpponent = value;
            }
        } 

        public IOpponent AwayOpponent 
        { 
            get => _visitorOpponent;
            set
            {
                if (!Opponents.Contains(value)) { throw new System.ArgumentException("Opponent must exist in set"); }
                if (HomeOpponent == value) { throw new System.ArgumentException("Opponent cannot be the same as HomeOpponent"); }

                _visitorOpponent = value;
            }
        }

        protected void NotifyTimeoutChange(IOpponent opponent, TimeoutManager.TimeoutChangeEvent e)
        {
            TimeoutChanged?.Invoke(opponent, e);
        }

        protected void NotifyPossessionChange(PossessionChangeEvent e)
        {
            PossessionChanged?.Invoke(this, e);
        }

        protected void NotifyScoreChange(IOpponent opponent, ScoreChangeEvent e)
        {
            ScoreChanged?.Invoke(opponent, e);
        }

        public void SetUsedTimeouts(IOpponent opponent, int usedTimeouts)
        {
            opponent.TimeoutManager.UsedTimeouts = usedTimeouts;
        }

        public int GetUsedTimeouts(IOpponent opponent)
        {
            return opponent.TimeoutManager.UsedTimeouts;
        }

        public int GetDivisionScore(IOpponent opponent, IDivision division) => opponent.GetDivisionScore(division);
        public void SetDivisionScore(int score, IOpponent opponent, IDivision division) => opponent.SetDivisionScore(score, division);

        public int GetScore(IOpponent opponent)
        {
            switch (ScoreMode)
            {
                case ScoringMode.LastScore:
                    return opponent.Scores.LastScore;
                case ScoringMode.SumScore:
                    return opponent.Scores.SumScore;
            }

            return 0;
        }

        public IDivision CurrentDivision 
        {
            get => DivisionStateMachine.CurrentDivision;
        }

        public bool AdvanceDivision() => DivisionStateMachine.Advance();
        public bool AdvanceDivisionIntoOvertime() => DivisionStateMachine.OvertimeAdvance();
        public bool FinalizeDivision() => DivisionStateMachine.Finalize();
    }
}