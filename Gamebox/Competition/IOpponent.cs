using System;
using System.Collections.Generic;
using Gamebox.Competition.Division;

namespace Gamebox.Competition
{
    public interface IOpponent
    {
        event EventHandler<PropertyChangeEvent> InformationChanged;
        event EventHandler<ScoreChangeEvent> ScoreChanged;

        string DisplayName { get; set; }
        string Nickname { get; set; }
        string CityName { get; set; }
        string StateName { get; set; }
        string TriCode { get; set; }
        ITimeoutManager TimeoutManager { get; }
        IScoreDictionary Scores { get; }
        void SetDivisionScore(int scoreValue, IDivision division);
        int GetDivisionScore(IDivision division);

        // TODO: add stats dict
    }
}