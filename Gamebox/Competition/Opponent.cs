using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Gamebox.Competition.Division;

namespace Gamebox.Competition
{
    public class Opponent : IOpponent
    {
        public Opponent(
            ITimeoutManager manager,
            IScoreDictionary scoreDictionary)
        {
            TimeoutManager = manager;
            Scores = scoreDictionary;
        }
        
        // TODO: fire event when properties change
        public string DisplayName { get; set; }
        public string TriCode { get; set; }
        public string Nickname { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public ITimeoutManager TimeoutManager { get; protected set; }
        public IScoreDictionary Scores { get; protected set; }
        public event EventHandler<PropertyChangeEvent> InformationChanged;
        public event EventHandler<ScoreChangeEvent> ScoreChanged;

        public int GetDivisionScore(IDivision division) => Scores[division];

        public void SetDivisionScore(int scoreValue, IDivision division)
        {
            var previousScore = Scores[division];
            Scores[division] = scoreValue;
            NotifyScoreChange(new ScoreChangeEvent(division, scoreValue, previousScore));
        }

        protected void NotifyScoreChange(ScoreChangeEvent e)
        {
            ScoreChanged?.Invoke(this, e);
        }

        protected void NotifyInformationChange(string propertyName)
        {
            InformationChanged?.Invoke(this, new PropertyChangeEvent(propertyName));
        }
    }
}