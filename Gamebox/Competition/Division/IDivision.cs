namespace Gamebox.Competition.Division
{
    public interface IDivision
    {
        string Name { get; set; }

        string ToString();
    }
}