using Stateless;
using System.Collections.Concurrent;
using System;

namespace Gamebox.Competition.Division
{
    public abstract class StateMachine : IStateMachine
    {
        public enum Trigger { Advance, OvertimeAdvance, Finalize }
        public event EventHandler<Stateless.StateMachine<IDivision, Trigger>.Transition> DivisionProgression;
        protected Stateless.StateMachine<IDivision, Trigger> _machine;
        public IDivision CurrentDivision { get; set; }

        public StateMachine()
        {
            _machine = new Stateless.StateMachine<IDivision, Trigger>(
                () => { return CurrentDivision; }, 
                (newDivision) => { CurrentDivision = newDivision; });
        }

        protected void NotifyDivisionProgression(Stateless.StateMachine<IDivision, Trigger>.Transition e)
        {
            DivisionProgression?.Invoke(this, e);
        }

        public string ToDotGraph()
        {
            return Stateless.Graph.UmlDotGraph.Format(_machine.GetInfo());
        }

        protected virtual bool TryFire(Trigger trigger)
        {
            if (!_machine.CanFire(trigger))
            {
                return false;
            }

            _machine.Fire(trigger);
            return true;
        }

        public bool Advance()
        {
            return TryFire(Trigger.Advance);
        }

        public bool OvertimeAdvance()
        {
            return TryFire(Trigger.OvertimeAdvance);
        }

        public bool Finalize()
        {
            return TryFire(Trigger.Finalize);
        }
    }
}