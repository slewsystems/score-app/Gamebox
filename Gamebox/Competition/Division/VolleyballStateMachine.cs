using System;

namespace Gamebox.Competition.Division
{
    public class VolleyballStateMachine : StateMachine
    {
        public VolleyballStateMachine(
            IDivision firstDivision, IDivision secondDivision, IDivision thirdDivision, IDivision fourthDivision, IDivision fifthDivision,
            IDivision finalDivision) : base()
        {
            _machine.Configure(firstDivision)
                .Permit(Trigger.Advance, secondDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(secondDivision)
                .Permit(Trigger.Advance, thirdDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(thirdDivision)
                .Permit(Trigger.Advance, fourthDivision)
                .Permit(Trigger.Finalize, finalDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(fourthDivision)
                .Permit(Trigger.Advance, fifthDivision)
                .Permit(Trigger.Finalize, finalDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(fifthDivision)
                .Permit(Trigger.Advance, finalDivision)
                .Permit(Trigger.Finalize, finalDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));
        }
    }
}