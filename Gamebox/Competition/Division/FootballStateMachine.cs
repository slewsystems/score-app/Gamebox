using System;

namespace Gamebox.Competition.Division
{
    public class FootballStateMachine : StateMachine
    {
        public FootballStateMachine(
            IDivision firstDivision, IDivision secondDivision, IDivision thirdDivision, IDivision fourthDivision,
            IDivision firstOvertimeDivision, IDivision secondOvertimeDivision, IDivision thirdOvertimeDivision, IDivision fourthOvertimeDivision, IDivision fifthOvertimeDivision,
            IDivision finalDivision, IDivision finalOvertimeDivision) : base()
        {
            _machine.Configure(firstDivision)
                .Permit(Trigger.Advance, secondDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(secondDivision)
                .Permit(Trigger.Advance, thirdDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(thirdDivision)
                .Permit(Trigger.Advance, fourthDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(fourthDivision)
                .Permit(Trigger.Advance, finalDivision)
                .Permit(Trigger.OvertimeAdvance, firstOvertimeDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(firstOvertimeDivision)
                .Permit(Trigger.Advance, finalOvertimeDivision)
                .Permit(Trigger.OvertimeAdvance, secondOvertimeDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(secondOvertimeDivision)
                .Permit(Trigger.Advance, finalOvertimeDivision)
                .Permit(Trigger.OvertimeAdvance, thirdOvertimeDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(thirdOvertimeDivision)
                .Permit(Trigger.Advance, finalOvertimeDivision)
                .Permit(Trigger.OvertimeAdvance, fourthOvertimeDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(fourthOvertimeDivision)
                .Permit(Trigger.Advance, finalOvertimeDivision)
                .Permit(Trigger.OvertimeAdvance, fifthOvertimeDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(fifthOvertimeDivision)
                .Permit(Trigger.Advance, finalOvertimeDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));
        }
    }
}