using System;

namespace Gamebox.Competition.Division
{
    public class SoccerStateMachine : StateMachine
    {
        public SoccerStateMachine(
            IDivision firstDivision, IDivision secondDivision,
            IDivision firstOvertimeDivision, IDivision secondOvertimeDivision,
            IDivision finalDivision, IDivision finalOvertimeDivision,
            IDivision penalityKicksDivision) : base()
        {
            _machine.Configure(firstDivision)
                .Permit(Trigger.Advance, secondDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(secondDivision)
                .Permit(Trigger.Advance, finalDivision)
                .Permit(Trigger.OvertimeAdvance, firstOvertimeDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(firstOvertimeDivision)
                .Permit(Trigger.Advance, finalOvertimeDivision)
                .Permit(Trigger.OvertimeAdvance, secondOvertimeDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(secondOvertimeDivision)
                .Permit(Trigger.Advance, finalOvertimeDivision)
                .Permit(Trigger.OvertimeAdvance, penalityKicksDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));

            _machine.Configure(penalityKicksDivision)
                .Permit(Trigger.Advance, finalOvertimeDivision)
                .OnExit((transition) => NotifyDivisionProgression(transition));
        }
    }
}