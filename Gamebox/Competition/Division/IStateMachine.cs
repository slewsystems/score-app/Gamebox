using System;

namespace Gamebox.Competition.Division
{
    public interface IStateMachine
    {
        IDivision CurrentDivision { get; set; }
        event EventHandler<Stateless.StateMachine<IDivision, StateMachine.Trigger>.Transition> DivisionProgression;
        bool Advance();
        bool OvertimeAdvance();

        bool Finalize();
    }
}