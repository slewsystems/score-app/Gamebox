using System;

namespace Gamebox.Competition.Division
{
    public class Division : IDivision
    {
        public string Name { get; set; }

        Division() {
            
        }

        public override string ToString()
        {
            return Name;
        }
    }
}