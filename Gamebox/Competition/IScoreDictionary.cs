using System.Collections.Generic;
using Gamebox.Competition.Division;

namespace Gamebox.Competition
{
    public interface IScoreDictionary : IEnumerable<int>
    {
        int LastScore { get; }
        int SumScore { get; }

        int this[IDivision division] { get; set; }
    }
}