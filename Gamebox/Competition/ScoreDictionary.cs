using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gamebox.Competition.Division;

namespace Gamebox.Competition
{
    public class ScoreDictionary : IScoreDictionary
    {
        public ScoreDictionary(IDictionary<IDivision, int> dictionary, IList<IDivision> order)
        {
            Dictionary = dictionary;
            DivisionOrder = order;
        }

        private IDictionary<IDivision, int> Dictionary { get; set; }
        private IList<IDivision> DivisionOrder { get; set; }

        public int this[IDivision division]
        {
            get => Dictionary[division];
            set 
            {
                Dictionary[division] = value;
                if (!DivisionOrder.Contains(division))
                {
                    DivisionOrder.Add(division);
                }
            }
        }

        public int LastScore 
        {
            get => this.Last();
        }

        public int SumScore
        {
            get => this.Aggregate((acc, score) => acc + score);
        }

        public IEnumerator<int> GetEnumerator()
        {
            foreach (IDivision division in DivisionOrder)
            {
                yield return Dictionary[division];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (IDivision division in DivisionOrder)
            {
                yield return Dictionary[division];
            }
        }
    }
}