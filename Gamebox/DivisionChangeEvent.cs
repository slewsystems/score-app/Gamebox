using Gamebox.Competition.Division;

namespace Gamebox
{
    public class DivisionChangeEvent
    {
        public DivisionChangeEvent(IDivision previousDivision, IDivision currentDivision, StateMachine.Trigger trigger)
        {
            PreviousDivision = previousDivision;
            CurrentDivision = currentDivision;
            Trigger = trigger;
        }

        public DivisionChangeEvent(Stateless.StateMachine<IDivision, StateMachine.Trigger>.Transition e) :
            this((IDivision) e.Source, (IDivision) e.Destination, e.Trigger) {}

        public IDivision PreviousDivision { get; protected set; }
        public IDivision CurrentDivision { get; protected set; }
        public StateMachine.Trigger Trigger { get; protected set; }
    }
}