namespace Gamebox
{
    public class PropertyChangeEvent
    {
        public PropertyChangeEvent(string propertyName)
        {
            PropertyName = propertyName;
        }

        public string PropertyName { get; protected set; }
    }
}