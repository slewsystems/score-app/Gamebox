using System;

namespace Gamebox
{
    public interface ITimeoutManager
    {
        event EventHandler<TimeoutManager.TimeoutChangeEvent> TimeoutChange;
        int MaxTimeouts { get; set; }
        int UsedTimeouts { get; set; }
        int RemainingTimeouts { get; }
        void ResetTimeouts();
    }
}