using System;

namespace Gamebox
{
    public class TimeoutManager : ITimeoutManager
    {
        public class TimeoutChangeEvent
        {
            public TimeoutChangeEvent(int previousUsedTimeouts, int currentUsedTimeouts, int currentRemainingTimeouts)
            {
                PreviousUsedTimeouts = previousUsedTimeouts;
                CurrentRemainingTimeouts = currentRemainingTimeouts;
                CurrentUsedTimeouts = currentUsedTimeouts;
            }

            public int PreviousUsedTimeouts { get; protected set; }
            public int CurrentUsedTimeouts { get; protected set; }
            public int CurrentRemainingTimeouts { get; protected set; }
        }

        public TimeoutManager(int maxTimeouts, int usedTimeouts = 0)
        {
            MaxTimeouts = maxTimeouts;
            UsedTimeouts = usedTimeouts;
        }

        private int _usedTimeouts = 0;
        public event EventHandler<TimeoutChangeEvent> TimeoutChange;
        public int MaxTimeouts { get; set; }
        public int UsedTimeouts 
        { 
            get { return _usedTimeouts; }
            set
            {
                if (value > MaxTimeouts) { throw new ArgumentException("UsedTimeouts must be less than or equal to MaxTimeouts"); }

                var previousValue = _usedTimeouts;
                _usedTimeouts = value;
                // no event fire 
                if (previousValue == _usedTimeouts) { return; }
                NotifyTimeoutChange(new TimeoutChangeEvent(previousValue, _usedTimeouts, RemainingTimeouts));
            }
        }

        public int RemainingTimeouts
        {
            get => Math.Max(MaxTimeouts - UsedTimeouts, 0);
        }

        public void ResetTimeouts() 
        {
            UsedTimeouts = 0;
        }

        protected void NotifyTimeoutChange(TimeoutChangeEvent e)
        {
            TimeoutChange?.Invoke(this, e);
        }

        // For unit testing
        public Delegate[] Subscribers { get => TimeoutChange?.GetInvocationList(); }
    }
}