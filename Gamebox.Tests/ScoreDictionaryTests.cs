using System;
using Xunit;
using Gamebox;
using Gamebox.Competition;
using Moq;
using Gamebox.Competition.Division;
using System.Collections.Generic;

namespace Gamebox.Tests
{
    public class ScoreDictionaryTests
    {
        [Fact]
        public void IteratorSet_ShouldAddDivisionToList_WhenSet()
        {
            var division = new Mock<IDivision>();
            var dict = new Dictionary<IDivision, int>();
            var list = new List<IDivision>();
            var scoreDictionary = new ScoreDictionary(dict, list);

            scoreDictionary[division.Object] = 0;

            Assert.Contains<IDivision>(division.Object, list);
        }

        [Fact]
        public void IteratorSet_ShouldNotAddDivisionToList_WhenAlreadySet()
        {
            var division = new Mock<IDivision>();
            var dict = new Dictionary<IDivision, int>();
            var list = new List<IDivision>();
            var scoreDictionary = new ScoreDictionary(dict, list);

            scoreDictionary[division.Object] = 0;
            scoreDictionary[division.Object] = 1;

            Assert.Equal(1, list.Count);
            Assert.Contains<IDivision>(division.Object, list);
        }

        [Fact]
        public void IteratorGet_ShouldBeInOrder_WhenIterated()
        {
            var divisionA = new Mock<IDivision>();
            var divisionB = new Mock<IDivision>();
            var dict = new Dictionary<IDivision, int>();
            var list = new List<IDivision>();
            var scoreDictionary = new ScoreDictionary(dict, list);

            scoreDictionary[divisionA.Object] = 0;
            scoreDictionary[divisionB.Object] = 1;

            Assert.Equal(divisionA.Object, list[0]);
            Assert.Equal(divisionB.Object, list[1]);
        }

        [Fact]
        public void IteratorSet_ShouldStoreValue_WhenSet()
        {
            var division = new Mock<IDivision>();
            var dict = new Dictionary<IDivision, int>();
            var list = new List<IDivision>();
            var scoreDictionary = new ScoreDictionary(dict, list);

            scoreDictionary[division.Object] = 5;

            Assert.Equal(5, scoreDictionary[division.Object]);
        }

        [Fact]
        public void IteratorSet_ShouldOverwriteValue_WhenSetTwice()
        {
            var division = new Mock<IDivision>();
            var dict = new Dictionary<IDivision, int>();
            var list = new List<IDivision>();
            var scoreDictionary = new ScoreDictionary(dict, list);

            scoreDictionary[division.Object] = 5;
            scoreDictionary[division.Object] = 10;

            Assert.Equal(10, scoreDictionary[division.Object]);
        }

        [Fact]
        public void LastScore_ShouldReturnLastValue_WhenCalled()
        {
            var divisionA = new Mock<IDivision>();
            var divisionB = new Mock<IDivision>();
            var dict = new Dictionary<IDivision, int>();
            var list = new List<IDivision>();
            var scoreDictionary = new ScoreDictionary(dict, list);

            scoreDictionary[divisionA.Object] = 5;
            scoreDictionary[divisionB.Object] = 10;

            Assert.Equal(10, scoreDictionary.LastScore);
        }

        [Fact]
        public void SumScore_ShouldReturnLastValue_WhenCalled()
        {
            var divisionA = new Mock<IDivision>();
            var divisionB = new Mock<IDivision>();
            var dict = new Dictionary<IDivision, int>();
            var list = new List<IDivision>();
            var scoreDictionary = new ScoreDictionary(dict, list);

            scoreDictionary[divisionA.Object] = 5;
            scoreDictionary[divisionB.Object] = 10;

            Assert.Equal(15, scoreDictionary.SumScore);
        }
    }
}