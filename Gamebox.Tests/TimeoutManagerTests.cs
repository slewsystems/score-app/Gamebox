using System;
using Xunit;
using Gamebox;

namespace Gamebox.Tests
{
    public class TimeoutManagerTests
    {
        [Theory, InlineData(3, 0, 3), InlineData(3, 3, 0)]
        public void RemainingTimeouts_ShouldCorrectlyCalculate_WhenCalled(int max, int used, int expectedRemaining)
        {
            var timeoutManager = new TimeoutManager(max, used);

            Assert.Equal(expectedRemaining, timeoutManager.RemainingTimeouts);
        }
        
        [Theory, InlineData(3, 4), InlineData(0, 1)]
        public void UsedTimeouts_ShouldIncrementUsedTimeouts_WhenValid(int initialUsed, int expectedUsed)
        {
            var timeoutManager = new TimeoutManager(5, initialUsed);

            timeoutManager.UsedTimeouts = initialUsed + 1;

            Assert.Equal(expectedUsed, timeoutManager.UsedTimeouts);
        }  

        [Fact]
        public void UsedTimeout_ShouldThrowException_WhenExceedsMaxTimeouts()
        {
            var timeoutManager = new TimeoutManager(1, 1);

            Assert.Throws<ArgumentException>(() => timeoutManager.UsedTimeouts = 2);
        }

        [Fact]
        public void ResetTimeouts_ShouldResetUsedTimeouts_WhenCalled() 
        {
            var timeoutManager = new TimeoutManager(1, 1);

            timeoutManager.ResetTimeouts();

            Assert.Equal(0, timeoutManager.UsedTimeouts);
        }

        [Fact]
        public void UsedTimeout_ShouldFireEvent_WhenChanged()
        {
            var timeoutManager = new TimeoutManager(2, 0);
            TimeoutManager.TimeoutChangeEvent ev = null;

            timeoutManager.TimeoutChange += (object sender, TimeoutManager.TimeoutChangeEvent e) => { ev = e; };

            timeoutManager.UsedTimeouts = 1;

            Assert.Equal(0, ev.PreviousUsedTimeouts);
            Assert.Equal(1, ev.CurrentRemainingTimeouts);
            Assert.Equal(1, ev.CurrentUsedTimeouts);
        }

        [Fact]
        public void UsedTimeout_ShouldNotFireEvent_WhenNotChanged()
        {
            var timeoutManager = new TimeoutManager(2, 1);
            TimeoutManager.TimeoutChangeEvent ev = null;

            timeoutManager.TimeoutChange += (object sender, TimeoutManager.TimeoutChangeEvent e) => { ev = e; };

            timeoutManager.UsedTimeouts = 1;

            Assert.Null(ev);
        }
    }
}