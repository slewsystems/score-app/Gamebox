using System;
using Xunit;
using Gamebox;
using Gamebox.Competition;
using Moq;
using Gamebox.Competition.Division;

namespace Gamebox.Tests
{
    public class OpponentTests
    {
        [Fact]
        public void SetDivisionScore_ShouldFireEvent_WhenChanged() {
            var timeoutManager = new Mock<ITimeoutManager>();
            var scoreDictionary = new Mock<IScoreDictionary>();
            var division = new Mock<IDivision>();
            var opponent = new Opponent(timeoutManager.Object, scoreDictionary.Object);
            ScoreChangeEvent ev = null;
            opponent.ScoreChanged += (object sender, ScoreChangeEvent e) => ev = e;

            opponent.SetDivisionScore(1, division.Object);

            Assert.Equal(division.Object, ev.Division);
            Assert.Equal(0, ev.PreviousScore);
            Assert.Equal(1, ev.CurrentScore);
        }

        [Fact]
        public void SetDivisionScore_ShouldSetScore_WhenSet()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var scoreDictionary = new Mock<IScoreDictionary>();
            scoreDictionary.SetupSet(m => m[It.IsAny<IDivision>()] = It.IsAny<int>()).Verifiable();
            var division = new Mock<IDivision>();
            var opponent = new Opponent(timeoutManager.Object, scoreDictionary.Object);

            opponent.SetDivisionScore(5, division.Object);

            scoreDictionary.VerifySet(m => m[division.Object] = 5, Times.Once());
        }

        [Fact]
        public void GetDivisionScore_ShouldFireEvent_WhenSet()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var scoreDictionary = new Mock<IScoreDictionary>();
            scoreDictionary.SetupGet(m => m[It.IsAny<IDivision>()]).Returns(10).Verifiable();
            var division = new Mock<IDivision>();
            var opponent = new Opponent(timeoutManager.Object, scoreDictionary.Object);

            var result = opponent.GetDivisionScore(division.Object);
            
            // TODO: figure this test out
            // scoreDictionary.VerifyGet(m => m[division.Object], Times.Once());
            Assert.Equal(10, result);
        }
    }
}