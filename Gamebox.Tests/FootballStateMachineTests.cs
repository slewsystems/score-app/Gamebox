using System;
using System.Collections.Generic;
using Xunit;
using Gamebox.Competition.Division;
using Moq;
using Stateless;

namespace Gamebox.Tests
{
    public class FootballStateMachineTests
    {
        static Mock<IDivision> d1 = new Mock<IDivision>(MockBehavior.Strict);
        static Mock<IDivision> d2 = new Mock<IDivision>(MockBehavior.Strict);
        static Mock<IDivision> d3 = new Mock<IDivision>(MockBehavior.Strict);
        static Mock<IDivision> d4 = new Mock<IDivision>(MockBehavior.Strict);
        static Mock<IDivision> ot1 = new Mock<IDivision>(MockBehavior.Strict);
        static Mock<IDivision> ot2 = new Mock<IDivision>(MockBehavior.Strict);
        static Mock<IDivision> ot3 = new Mock<IDivision>(MockBehavior.Strict);
        static Mock<IDivision> ot4 = new Mock<IDivision>(MockBehavior.Strict);
        static Mock<IDivision> ot5 = new Mock<IDivision>(MockBehavior.Strict);
        static Mock<IDivision> fin = new Mock<IDivision>(MockBehavior.Strict);
        static Mock<IDivision> otFin = new Mock<IDivision>(MockBehavior.Strict);

        static IEnumerable<object[]> AdvanceDivisions()
        {
            yield return new object[] { d1, d2, StateMachine.Trigger.Advance };
            yield return new object[] { d2, d3, StateMachine.Trigger.Advance };
            yield return new object[] { d3, d4, StateMachine.Trigger.Advance };
            yield return new object[] { d4, fin, StateMachine.Trigger.Advance };
            yield return new object[] { ot1, otFin, StateMachine.Trigger.Advance };
            yield return new object[] { ot2, otFin, StateMachine.Trigger.Advance };
            yield return new object[] { ot3, otFin, StateMachine.Trigger.Advance };
            yield return new object[] { ot4, otFin, StateMachine.Trigger.Advance };
            yield return new object[] { ot5, otFin, StateMachine.Trigger.Advance };
        }

        static IEnumerable<object[]> OvertimeAdvanceDivisions()
        {
            yield return new object[] { d4, ot1, StateMachine.Trigger.OvertimeAdvance };
            yield return new object[] { ot1, ot2, StateMachine.Trigger.OvertimeAdvance };
            yield return new object[] { ot2, ot3, StateMachine.Trigger.OvertimeAdvance };
            yield return new object[] { ot3, ot4, StateMachine.Trigger.OvertimeAdvance };
            yield return new object[] { ot4, ot5, StateMachine.Trigger.OvertimeAdvance };
        }

        static IEnumerable<object[]> NotAdvanceDivisions()
        {
            yield return new object[] { fin, StateMachine.Trigger.Advance };
            yield return new object[] { otFin, StateMachine.Trigger.Advance };
        }

        static IEnumerable<object[]> NotOvertimeAdvanceDivisions()
        {
            yield return new object[] { d1, StateMachine.Trigger.OvertimeAdvance };
            yield return new object[] { d2, StateMachine.Trigger.OvertimeAdvance };
            yield return new object[] { d3, StateMachine.Trigger.OvertimeAdvance };
            yield return new object[] { fin, StateMachine.Trigger.OvertimeAdvance };
            yield return new object[] { otFin, StateMachine.Trigger.OvertimeAdvance };
        }

        private FootballStateMachine BuildStateMachine(Mock<IDivision> startDivision)
        {
            return new FootballStateMachine(d1.Object, d2.Object, d3.Object, d4.Object, ot1.Object, ot2.Object, ot3.Object, ot4.Object, ot5.Object, fin.Object, otFin.Object)
            {
                CurrentDivision = startDivision.Object
            };
        }

        [Fact]
        public void Initialization_ShouldStartWithDivision_Always() 
        {
            var machine = BuildStateMachine(d1);
            
            Assert.Equal(machine.CurrentDivision, d1.Object);
        }

        [Theory, MemberData(nameof(AdvanceDivisions))]
        public void Advanace_ShouldAdvance_WhenCalled(Mock<IDivision> startDivision, Mock<IDivision> nextDivision, StateMachine.Trigger trigger)
        {
            var machine = BuildStateMachine(startDivision);
            
            var result = machine.Advance();
            
            Assert.Equal(machine.CurrentDivision, nextDivision.Object);
            Assert.True(result);
        }

        [Theory, MemberData(nameof(AdvanceDivisions))]
        public void Advance_ShouldFireEvent_WhenCalled(Mock<IDivision> startDivision, Mock<IDivision> nextDivision, StateMachine.Trigger trigger)
        {
            var machine = BuildStateMachine(startDivision);
            Stateless.StateMachine<IDivision, StateMachine.Trigger>.Transition ev = null;

            machine.DivisionProgression += (object sender, Stateless.StateMachine<IDivision, StateMachine.Trigger>.Transition e) => { ev = e; };

            var result = machine.Advance();
            var source = ev.Source;
            var destination = ev.Destination;

            Assert.Equal((IDivision)source, startDivision.Object);
            Assert.Equal((IDivision)destination, nextDivision.Object);
            Assert.Equal(ev.Trigger, trigger);
        }

        [Theory, MemberData(nameof(OvertimeAdvanceDivisions))]
        public void OvertimeAdvance_ShouldFireEvent_WhenCalled(Mock<IDivision> startDivision, Mock<IDivision> nextDivision, StateMachine.Trigger trigger)
        {
            var machine = BuildStateMachine(startDivision);
            Stateless.StateMachine<IDivision, StateMachine.Trigger>.Transition ev = null;

            machine.DivisionProgression += (object sender, Stateless.StateMachine<IDivision, StateMachine.Trigger>.Transition e) => { ev = e; };

            var result = machine.OvertimeAdvance();
            var source = ev.Source;
            var destination = ev.Destination;

            Assert.Equal((IDivision)source, startDivision.Object);
            Assert.Equal((IDivision)destination, nextDivision.Object);
            Assert.Equal(ev.Trigger, trigger);
        }

        [Theory, MemberData(nameof(NotAdvanceDivisions))]
        public void Advanace_ShouldNotAdvance_WhenCalled(Mock<IDivision> startDivision, StateMachine.Trigger trigger)
        {
            var machine = BuildStateMachine(startDivision);
            
            var result = machine.Advance();
            
            Assert.Equal(machine.CurrentDivision, startDivision.Object);
            Assert.False(result);
        }

        [Theory, MemberData(nameof(NotOvertimeAdvanceDivisions))]
        public void OvertimeAdvance_ShouldNotAdvance_WhenCalled(Mock<IDivision> startDivision, StateMachine.Trigger trigger)
        {
            var machine = BuildStateMachine(startDivision);
            
            var result = machine.OvertimeAdvance();
            
            Assert.Equal(machine.CurrentDivision, startDivision.Object);
            Assert.False(result);
        }

        [Theory, MemberData(nameof(OvertimeAdvanceDivisions))]
        public void OvertimeAdvance_ShoulAdvance_WhenCalled(Mock<IDivision> startDivision, Mock<IDivision> nextDivision, StateMachine.Trigger trigger)
        {
            var machine = BuildStateMachine(startDivision);
            
            var result = machine.OvertimeAdvance();
            
            Assert.Equal(machine.CurrentDivision, nextDivision.Object);
            Assert.True(result);
        }
    }
}