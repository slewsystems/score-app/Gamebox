using System;
using Xunit;
using System.Collections.Generic;
using Gamebox.Competition;
using Gamebox.Competition.Division;
using Moq;

namespace Gamebox.Tests
{
    public class CompetitionTests
    {
        [Fact]
        public void Opponents_ShowThrowErrorWhenNotEmpty_WhenInitializing()
        {
            var opponent = new Mock<IOpponent>();
            var opponentSet = new HashSet<IOpponent>() { opponent.Object };
            var stateMachine = new Mock<IStateMachine>();

            Assert.Throws<ArgumentException>(() => new Competition.Competition(opponentSet, stateMachine.Object));
        }

        [Fact]
        public void HomeOpponent_ShouldBeSet_WhenInSet()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var opponent = new Mock<IOpponent>();
            opponent.SetupGet(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);
            competition.AddOpponent(opponent.Object);

            competition.HomeOpponent = opponent.Object;

            Assert.Equal(competition.HomeOpponent, opponent.Object);
        }

        [Fact]
        public void AwayOpponent_ShouldBeSet_WhenInSet()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var opponent = new Mock<IOpponent>();
            opponent.SetupGet(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);
            competition.AddOpponent(opponent.Object);

            competition.AwayOpponent = opponent.Object;

            Assert.Equal(competition.AwayOpponent, opponent.Object);
        }

        [Fact]
        public void HomeOpponent_ShouldThrowException_WhenNotInSet()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var opponent = new Mock<IOpponent>();
            opponent.SetupGet(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);

            Assert.Throws<System.ArgumentException>(() => competition.HomeOpponent = opponent.Object);
        }

        [Fact]
        public void AwayOpponent_ShouldThrowException_WhenNotInSet()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var opponent = new Mock<IOpponent>();
            opponent.SetupGet(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);

            Assert.Throws<System.ArgumentException>(() => competition.AwayOpponent = opponent.Object);
        }

        [Fact]
        public void AwayOpponent_ShouldThrowException_WhenSetAsHomeOpponent()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var opponent = new Mock<IOpponent>();
            opponent.SetupGet(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);
            competition.AddOpponent(opponent.Object);

            competition.HomeOpponent = opponent.Object;

            Assert.Throws<System.ArgumentException>(() => competition.AwayOpponent = opponent.Object);
        }

        [Fact]
        public void HomeOpponent_ShouldThrowException_WhenSetAsAwayOpponent()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var opponent = new Mock<IOpponent>();
            opponent.SetupGet(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);
            competition.AddOpponent(opponent.Object);

            competition.AwayOpponent = opponent.Object;

            Assert.Throws<System.ArgumentException>(() => competition.HomeOpponent = opponent.Object);
        }

        [Fact]        
        public void Opponents_ShouldBindToTimeoutEvents_WhenAssigned()
        {
            var timeoutManager = new TimeoutManager(0, 0);
            var opponent = new Mock<IOpponent>();
            opponent.SetupGet(m => m.TimeoutManager).Returns(timeoutManager);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();

            // is null until you bind the first listener
            Assert.Null(timeoutManager.Subscribers);
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);
            competition.AddOpponent(opponent.Object);
            Assert.Equal(1, timeoutManager.Subscribers.Length);
        }

        [Fact]
        public void SetUsedTimeouts_ShouldCallOpponentTimeoutManager_Always()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var opponent = new Mock<IOpponent>();
            opponent.Setup(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);
            competition.AddOpponent(opponent.Object);

            competition.SetUsedTimeouts(opponent.Object, 0);

            timeoutManager.VerifySet(m => m.UsedTimeouts = It.IsAny<int>());
        }

        [Fact]
        public void GetUsedTimeouts_ShouldCallOpponentTimeoutManager_Always()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            timeoutManager.SetupGet(m => m.UsedTimeouts).Returns(1);
            var opponent = new Mock<IOpponent>();
            opponent.Setup(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);
            competition.AddOpponent(opponent.Object);

            var result = competition.GetUsedTimeouts(opponent.Object);

            timeoutManager.VerifyGet(m => m.UsedTimeouts);
            Assert.Equal(1, result);
        }

        [Fact]
        public void OpponentWithPossession_ShouldThrowException_WhenOpponentNotInSet()
        {
            var opponent = new Mock<IOpponent>();
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);

            Assert.Throws<System.ArgumentException>(() => competition.OpponentWithPossession = opponent.Object);
        }

        [Fact]
        public void OpponentWithPossession_ShouldSetOpponentWithPossession_WhenOpponentInSet()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var opponent = new Mock<IOpponent>();
            opponent.Setup(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);
            competition.AddOpponent(opponent.Object);

            competition.OpponentWithPossession = opponent.Object;

            Assert.Equal(opponent.Object, competition.OpponentWithPossession);
        }

        [Fact]
        public void OpponentWithPossession_ShouldFireEvent_WhenChanged()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var opponent = new Mock<IOpponent>();
            opponent.Setup(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);
            competition.AddOpponent(opponent.Object);
            PossessionChangeEvent ev = null;
            competition.PossessionChanged += (object sender, PossessionChangeEvent e) => ev = e;

            competition.OpponentWithPossession = opponent.Object;

            Assert.Equal(null, ev.PreviousOpponent);
            Assert.Equal(opponent.Object, ev.CurrentOpponent);
        }

        [Fact]
        public void OpponentWithPossession_ShouldNotFireEvent_WhenNotChanged()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var opponent = new Mock<IOpponent>();
            opponent.Setup(m => m.TimeoutManager).Returns(timeoutManager.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);
            competition.AddOpponent(opponent.Object);

            competition.OpponentWithPossession = opponent.Object; // set first
            PossessionChangeEvent ev = null;
            competition.PossessionChanged += (object sender, PossessionChangeEvent e) => ev = e;
            competition.OpponentWithPossession = opponent.Object; // set again

            Assert.Null(ev);
        }

        [Fact]
        public void Opponent_ShouldFireEvent_WhenScoreSet()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var scoreDictionary = new Mock<IScoreDictionary>();
            var division = new Mock<IDivision>();
            var opponent = new Opponent(timeoutManager.Object, scoreDictionary.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);

            competition.AddOpponent(opponent);
            ScoreChangeEvent ev = null;
            object send = null;
            competition.ScoreChanged += (object sender, ScoreChangeEvent e) => { ev = e; send = sender; };
            competition.SetDivisionScore(10, opponent, division.Object);

            Assert.Equal(10, ev.CurrentScore);
            Assert.Equal(0, ev.PreviousScore);
            Assert.Equal(opponent, send);
        }

        [Fact]
        public void GetScore_ShouldReturnLastScore_WhenScoreModeSetToLast()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var scoreDictionary = new Mock<IScoreDictionary>();
            scoreDictionary.SetupGet(m => m.LastScore).Returns(10);
            var divisionA = new Mock<IDivision>();
            var divisionB = new Mock<IDivision>();
            var opponent = new Opponent(timeoutManager.Object, scoreDictionary.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);

            competition.ScoreMode = Competition.Competition.ScoringMode.LastScore;
            competition.AddOpponent(opponent);

            Assert.Equal(10, competition.GetScore(opponent));
        }

        [Fact]
        public void GetScore_ShouldReturnSummedScore_WhenScoreModeSetToSum()
        {
            var timeoutManager = new Mock<ITimeoutManager>();
            var scoreDictionary = new Mock<IScoreDictionary>();
            scoreDictionary.SetupGet(m => m.SumScore).Returns(10);
            var divisionA = new Mock<IDivision>();
            var divisionB = new Mock<IDivision>();
            var opponent = new Opponent(timeoutManager.Object, scoreDictionary.Object);
            var opponentSet = new HashSet<IOpponent>();
            var stateMachine = new Mock<IStateMachine>();
            var competition = new Competition.Competition(opponentSet, stateMachine.Object);

            competition.ScoreMode = Competition.Competition.ScoringMode.SumScore;
            competition.AddOpponent(opponent);

            Assert.Equal(10, competition.GetScore(opponent));
        }
    }
}