# Gamebox

Manages a game/match/competition's state as well as tracking stats of players/members and overall game progression. This library is intended to support the following sports (work in progress):

- US OHSAA Basketball
- US OHSAA Baseball
- US OHSAA Football
- US OHSAA Soccer
- US OHSAA Lacrosse
- US OHSAA Wrestling
- US OHSAA Volleyball

## Terminology

In order to keep this library agnostic to any specific sport we try to use generic terms. Below you will find a list of the terminology used in this library along with what you may be used to using:

Gamebox Term|Alias|Reasoning
--|--|--
Competition|Sport, Game, Match|Using the terms "game" or "match" can denote the origin of the sport. A sport that is a "game" is of U.S. origins, while a sport that is a "match" is of U.K. origins.
Opponent|Team|Using the term "team" can make an assumption that a "team" consists of many players ("there is no I in team"). While most competitions are a "players vs. players" strategy other competitions can be "one vs. one." The idea is an "opponent" can consist of one or many "members."
Member|Player, Staff|This in an opinionated decision. "player" may denote a human player and not all sports are required to be human(s) vs. human(s).
Venue|Field, Court, Stadium, Arena|The definition of "field" is an area that is open while a "court" or "arena" is an enclosed space.
CompetitionDivision|Inning, Set, Quarter, Period, Frame|It seems like every sport has its own terminology here. All to denote a division or break in a "competition."
